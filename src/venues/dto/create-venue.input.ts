import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class CreateVenueInput {
  @Field(() => String, { description: 'Venue Name' })
  name: string;

  @Field(() => String, { description: 'Venue Location' })
  location: string;
}
