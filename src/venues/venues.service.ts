import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Venue } from './entities/venue.entity';
import { CreateVenueInput } from './dto/create-venue.input';
import { UpdateVenueInput } from './dto/update-venue.input';

@Injectable()
export class VenuesService {
  constructor(
    @InjectRepository(Venue) private venuesRepository: Repository<Venue>,
  ) {}

  async create(createVenueInput: CreateVenueInput): Promise<Venue> {
    const newVenue = this.venuesRepository.create(createVenueInput);
    return this.venuesRepository.save(newVenue);
  }

  async findAll(): Promise<Venue[]> {
    return this.venuesRepository.find();
  }

  async findOne(id: number): Promise<Venue> {
    return this.venuesRepository.findOneOrFail(id);
  }

  async update(updateVenueInput: UpdateVenueInput) {
    const newVenue = this.venuesRepository.create(updateVenueInput);
    return this.venuesRepository.save(newVenue);
  }

  async remove(id: number) {
    return this.venuesRepository.delete(id);
  }
}
