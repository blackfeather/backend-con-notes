import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { VenuesService } from './venues.service';
import { Venue } from './entities/venue.entity';
import { CreateVenueInput } from './dto/create-venue.input';
import { UpdateVenueInput } from './dto/update-venue.input';

@Resolver(() => Venue)
export class VenuesResolver {
  constructor(private readonly venuesService: VenuesService) {}

  @Mutation(() => Venue)
  createVenue(@Args('createVenueInput') createVenueInput: CreateVenueInput) {
    return this.venuesService.create(createVenueInput);
  }

  @Query(() => [Venue], { name: 'venues' })
  findAll() {
    return this.venuesService.findAll();
  }

  @Query(() => Venue, { name: 'venue' })
  findOne(@Args('id', { type: () => Int }) id: number) {
    return this.venuesService.findOne(id);
  }

  @Mutation(() => Venue)
  updateVenue(@Args('updateVenueInput') updateVenueInput: UpdateVenueInput) {
    return this.venuesService.update(updateVenueInput);
  }

  @Mutation(() => Venue)
  removeVenue(@Args('id', { type: () => Int }) id: number) {
    return this.venuesService.remove(id);
  }
}
