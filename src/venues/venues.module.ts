import { Module } from '@nestjs/common';
import { VenuesService } from './venues.service';
import { VenuesResolver } from './venues.resolver';
import { Venue } from './entities/venue.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Venue])],
  providers: [VenuesResolver, VenuesService],
  exports: [VenuesService],
  
})
export class VenuesModule {}
