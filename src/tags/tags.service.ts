import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTagInput } from './dto/create-tag.input'
import { UpdateTagInput } from './dto/update-tag.input';
import { Tag } from './entities/tag.entity';

@Injectable()
export class TagsService {
  constructor(@InjectRepository(Tag) private tagsRepository: Repository<Tag>) {}

  async findOne(id: number): Promise<Tag> {
    return this.tagsRepository.findOneOrFail(id);
  }

  async update(updateTagInput: UpdateTagInput) {
    const newTag = this.tagsRepository.create(updateTagInput);
    return this.tagsRepository.save(newTag);
  }

  async remove(id: number) {
    return `This action removes a #${id} tag`;
  }

  async create(createTagInput: CreateTagInput): Promise<Tag> {
    const newTag = this.tagsRepository.create(createTagInput);
    return this.tagsRepository.save(newTag);
  }

  async findAll(): Promise<Tag[]> {
    return this.tagsRepository.find();
  }
}
