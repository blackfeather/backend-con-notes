import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { PanelsService } from './panels.service';
import { Panel } from './entities/panel.entity';
import { CreatePanelInput } from './dto/create-panel.input';
import { UpdatePanelInput } from './dto/update-panel.input';

@Resolver(() => Panel)
export class PanelsResolver {
  constructor(private readonly panelsService: PanelsService) {}

  @Mutation(() => Panel)
  createPanel(@Args('createPanelInput') createPanelInput: CreatePanelInput) {
    return this.panelsService.create(createPanelInput);
  }

  @Query(() => [Panel], { name: 'panels' })
  findAll() {
    return this.panelsService.findAll();
  }

  @Query(() => Panel, { name: 'panel' })
  findOne(@Args('id', { type: () => Int }) id: number) {
    return this.panelsService.findOne(id);
  }

  @Mutation(() => Panel)
  updatePanel(@Args('updatePanelInput') updatePanelInput: UpdatePanelInput) {
    return this.panelsService.update(updatePanelInput);
  }

  @Mutation(() => Panel)
  removePanel(@Args('id', { type: () => Int }) id: number) {
    return this.panelsService.remove(id);
  }
}
