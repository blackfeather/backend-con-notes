import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AffairsService } from 'src/affairs/affairs.service';
import { Affair } from 'src/affairs/entities/affair.entity';
import { Repository } from 'typeorm';
import { CreatePanelInput } from './dto/create-panel.input';
import { UpdatePanelInput } from './dto/update-panel.input';
import { Panel } from './entities/panel.entity';

@Injectable()
export class PanelsService {
  constructor(
    @InjectRepository(Panel) private panelsRepository: Repository<Panel>,
    private affairsService: AffairsService,
  ) {}

  async create(createPanelInput: CreatePanelInput): Promise<Panel> {
    const newPanel = this.panelsRepository.create(createPanelInput);
    return this.panelsRepository.save(newPanel);
  }

  async findAll(): Promise<Panel[]> {
    return this.panelsRepository.find();
  }

  async findOne(id: number): Promise<Panel> {
    return this.panelsRepository.findOneOrFail(id);
  }

  async update(updatePanelInput: UpdatePanelInput) {
    const newPanel = this.panelsRepository.create(updatePanelInput);
    return this.panelsRepository.save(newPanel);
  }

  async remove(id: number) {
    return this.panelsRepository.delete(id);
  }

  async getAffair(affairId: number): Promise<Affair> {
    return this.affairsService.findOne(affairId);
  }
}
