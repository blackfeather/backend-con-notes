import { Module } from '@nestjs/common';
import { PanelsService } from './panels.service';
import { PanelsResolver } from './panels.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Panel } from './entities/panel.entity';
import { AffairsModule } from 'src/affairs/affairs.module';

@Module({
  imports: [TypeOrmModule.forFeature([Panel]), AffairsModule],
  providers: [PanelsResolver, PanelsService],
  exports: [PanelsService],
})
export class PanelsModule {}
