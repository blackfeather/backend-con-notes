import { ObjectType, Field, Int, InputType } from '@nestjs/graphql';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Affair } from 'src/affairs/entities/affair.entity';

@Entity()
@ObjectType()
export class Panel {
  @PrimaryGeneratedColumn()
  @Field((type) => Int)
  id: number;

  @Column()
  @Field(() => String, { description: 'Panel Title' })
  name: string;

  @Column()
  @Field(() => String, { description: 'Panel notes' })
  notes: string;

  @Column()
  @Field(() => Int)
  affairId?: number;

  // add: [Tag], [Presenter]
  @ManyToOne(() => Affair, (affair) => affair.panels)
  @Field((type) => Affair)
  affair: Affair;
}
