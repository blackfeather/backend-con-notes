import { CreatePanelInput } from './create-panel.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdatePanelInput extends PartialType(CreatePanelInput) {
  @Field(() => Int)
  id: number;
}
