import { InputType, Field, Int } from '@nestjs/graphql';
import { Affair } from 'src/affairs/entities/affair.entity';

@InputType()
export class CreatePanelInput {
  @Field(() => String, { description: 'Panel Title' })
  name: string;

  @Field(() => String, { description: 'Panel notes' })
  notes: string;

  @Field(() => Affair, { description: 'Affair where panel was presented.' })
  affair: Affair;
}
