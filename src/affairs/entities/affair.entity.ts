import { ObjectType, Field, Int } from '@nestjs/graphql';
import { Panel } from 'src/panels/entities/panel.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
@ObjectType()
export class Affair {
  @PrimaryGeneratedColumn()
  @Field((type) => Int)
  id: number;

  @Column()
  @Field(() => String, { description: 'Affair Name' })
  name: string;

  @Column()
  @Field(() => String, { description: 'When the affair started.' })
  started: string;

  @Column()
  @Field(() => String, { description: 'When the affair ended.' })
  ended: string;

  @OneToMany(() => Panel, (panel) => panel.affair)
  @Field((type) => [Panel], { nullable: true })
  panels?: Panel[];
}
