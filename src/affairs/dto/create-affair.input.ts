import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class CreateAffairInput {
  @Field(() => String, { description: 'Affair Name' })
  name: string;

  @Field(() => String, { description: 'Affair Start time and Date' })
  started: string;

  @Field(() => String, { description: 'Affair End time and Date' })
  ended: string;
}
