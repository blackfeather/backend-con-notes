import { CreateAffairInput } from './create-affair.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateAffairInput extends PartialType(CreateAffairInput) {
  @Field(() => Int)
  id: number;
}
