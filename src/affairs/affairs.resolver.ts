import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { AffairsService } from './affairs.service';
import { Affair } from './entities/affair.entity';
import { CreateAffairInput } from './dto/create-affair.input';
import { UpdateAffairInput } from './dto/update-affair.input';

@Resolver(() => Affair)
export class AffairsResolver {
  constructor(private readonly affairsService: AffairsService) {}

  @Mutation(() => Affair)
  createAffair(
    @Args('createAffairInput') createAffairInput: CreateAffairInput,
  ) {
    return this.affairsService.create(createAffairInput);
  }

  @Query(() => [Affair], { name: 'affairs' })
  findAll() {
    return this.affairsService.findAll();
  }

  @Query(() => Affair, { name: 'affair' })
  getAffair(@Args('id', { type: () => Int }) id: number): Promise<Affair> {
    return this.affairsService.findOne(id);
  }

  @Mutation(() => Affair)
  updateAffair(
    @Args('updateAffairInput') updateAffairInput: UpdateAffairInput,
  ) {
    return this.affairsService.update(updateAffairInput);
  }

  @Mutation(() => Affair)
  removeAffair(@Args('id', { type: () => Int }) id: number) {
    return this.affairsService.remove(id);
  }
}
