import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateAffairInput } from './dto/create-affair.input';
import { UpdateAffairInput } from './dto/update-affair.input';
import { Affair } from './entities/affair.entity';

@Injectable()
export class AffairsService {
  constructor(
    @InjectRepository(Affair) private affairsRepository: Repository<Affair>,
  ) {}

  async findOne(id: number): Promise<Affair> {
    return this.affairsRepository.findOneOrFail(id);
  }

  async update(updateAffairInput: UpdateAffairInput) {
    const newAffair = this.affairsRepository.create(updateAffairInput);
    return this.affairsRepository.save(newAffair);
  }

  async remove(id: number) {
    return this.affairsRepository.delete(id);
  }

  async create(createAffairInput: CreateAffairInput): Promise<Affair> {
    const newAffair = this.affairsRepository.create(createAffairInput);
    return this.affairsRepository.save(newAffair);
  }

  async findAll(): Promise<Affair[]> {
    return this.affairsRepository.find();
  }
}
