import { Module } from '@nestjs/common';
import { AffairsService } from './affairs.service';
import { AffairsResolver } from './affairs.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Affair } from './entities/affair.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Affair])],
  providers: [AffairsResolver, AffairsService],
  exports: [AffairsService],
})
export class AffairsModule {}
