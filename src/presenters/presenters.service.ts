import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreatePresenterInput } from './dto/create-presenter.input';
import { UpdatePresenterInput } from './dto/update-presenter.input';
import { Presenter } from './entities/presenter.entity';

@Injectable()
export class PresentersService {
  constructor(
    @InjectRepository(Presenter) private presentersRepository: Repository<Presenter>,
  ) {}

  async create(createPresenterInput: CreatePresenterInput): Promise<Presenter> {
    const newPresenter = this.presentersRepository.create(createPresenterInput);
    return this.presentersRepository.save(newPresenter);
  }

  async findAll(): Promise<Presenter[]> {
    return this.presentersRepository.find();
  }

  async findOne(id: number): Promise<Presenter> {
    return this.presentersRepository.findOneOrFail(id);
  }

  async update(updatePresenterInput: UpdatePresenterInput) {
    const newPresenter = this.presentersRepository.create(updatePresenterInput);
    return this.presentersRepository.save(newPresenter);
  }

  async remove(id: number) {
    return this.presentersRepository.delete(id);
  }
}
