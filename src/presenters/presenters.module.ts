import { Module } from '@nestjs/common';
import { PresentersService } from './presenters.service';
import { PresentersResolver } from './presenters.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Presenter } from './entities/presenter.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Presenter])],
  providers: [PresentersResolver, PresentersService],
  exports: [PresentersService],
})
export class PresentersModule {}
