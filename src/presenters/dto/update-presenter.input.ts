import { CreatePresenterInput } from './create-presenter.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdatePresenterInput extends PartialType(CreatePresenterInput) {
  @Field(() => Int)
  id: number;
}
