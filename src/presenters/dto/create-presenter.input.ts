import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class CreatePresenterInput {
  @Field(() => String, { description: 'presenter name' })
  name: string;

  @Field(() => String, { description: 'presenter URI or URL' })
  uri: string;
}
