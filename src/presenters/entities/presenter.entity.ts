import { ObjectType, Field, Int } from '@nestjs/graphql';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
@ObjectType()
export class Presenter {
  @PrimaryGeneratedColumn()
  @Field(type => Int)
  id: number;

  @Column()
  @Field(() => String, { description: 'Presenter Name' })
  name: string;

  @Column()
  @Field(() => String, { description: 'Presenter Name' })
  uri: string;
}