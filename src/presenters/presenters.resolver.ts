import { Args, Int, Mutation, Query, Resolver } from '@nestjs/graphql';
import { PresentersService } from './presenters.service';
import { Presenter } from './entities/presenter.entity';
import { CreatePresenterInput } from './dto/create-presenter.input';
import { UpdatePresenterInput } from './dto/update-presenter.input';

@Resolver(() => Presenter)
export class PresentersResolver {
  constructor(private readonly presentersService: PresentersService) {}

  @Mutation(() => Presenter)
  createPresenter(
    @Args('createPresenterInput') createPresenterInput: CreatePresenterInput,
  ) {
    return this.presentersService.create(createPresenterInput);
  }

  @Query(() => [Presenter], { name: 'presenters' })
  findAll() {
    return this.presentersService.findAll();
  }

  @Query(() => Presenter, { name: 'presenter' })
  findOne(@Args('id', { type: () => Int }) id: number) {
    return this.presentersService.findOne(id);
  }

  @Mutation(() => Presenter)
  updatePresenter(
    @Args('updatePresenterInput') updatePresenterInput: UpdatePresenterInput,
  ) {
    return this.presentersService.update(updatePresenterInput);
  }

  @Mutation(() => Presenter)
  removePresenter(@Args('id', { type: () => Int }) id: number) {
    return this.presentersService.remove(id);
  }
}
